<?php 
  require_once("headerpage.php");
?>
    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Program Gereja
        <small>GBI Bukit Anugerah Santunan Janda-Janda</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Program Gereja Santunan Janda-Janda</li>
      </ol>

      <!-- Portfolio Item Row -->
      <div class="row">

        <div class="col-md-8">
        <img class="img-fluid" src="img/janda2.jpg" alt="">
        </div>

        <div class="col-md-4">
          <h3 class="my-3">Santunan Janda-Janda</h3>
          <p>Dibuatnya pelayanan santunan untuk janda dilatar belakangi oleh keadaan para janda yang tidak mampu kurang mendapatkan perhatian, baik dari lingkungan maupun pemerintah.</p>
          <h3 class="my-3">Tujuan Program</h3>
          <ul>
            <li>Supaya janda yang tidak mampu di Gunungkidul tetap bisa bahagia di masa tuanya sampai akhir hayatnya.</li>
          </ul>
        </div>

      </div>
      <!-- /.row -->

      <!-- Related Projects Row -->
      <h3 class="my-4">Dokumentasi Kegiatan</h3>

      <div class="row">

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/janda1.jpg" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/janda3.jpg" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/janda4.jpg" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/janda5.jpg" alt="">
          </a>
        </div>

      </div>
      <!-- /.row -->

    </div>
    </div>
    <!-- /.container -->

    <?php 
    require_once("footerpage.php");
?>
