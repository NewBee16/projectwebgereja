<?php 
  require_once("headerpage.php");
?>
    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Program Gereja
        <small>GBI Bukit Anugerah Warung Sedekah</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Program Gereja Warung Sedekah</li>
      </ol>

      <!-- Portfolio Item Row -->
      <div class="row">

        <div class="col-md-8">
        <img class="img-fluid" src="img/wr7.jpg" alt="">
        </div>

        <div class="col-md-4">
          <h3 class="my-3">Warung Sedekah</h3>
          <p>Dibuatnya pelayanan Warung Sedekah, dilatar belakangi oleh keprihatinan hamba Tuhan dan Gereja, melihat masyarakat yang menurun tingkat kesehatan.</p>
          <h3 class="my-3">Tujuan Program</h3>
          <ul>
            <li>Meningkatkan gizi dan kualitas kehidupan Kaum Dhuafa di Gunungkidul.</li>
          </ul>
        </div>

      </div>
      <!-- /.row -->

      <!-- Related Projects Row -->
      <h3 class="my-4">Dokumentasi Kegiatan</h3>

      <div class="row">

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/wr1.jpg" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/wr2.jpg" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/wr3.jpg" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/wr4.jpg" alt="">
          </a>
        </div>

      </div>
      <!-- /.row -->

    </div>
    </div>
    <!-- /.container -->

<?php 
    require_once("footerpage.php");
?>
