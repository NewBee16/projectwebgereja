<?php 
    require_once("headerpage.php");
?>

<body>
    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">
        <small>Visi & Misi </small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Visi & Misi GBI Bukit Anugerah</li>
      </ol>

      <!-- Portfolio Item Row -->
      <div class="row">

        
        <div class="col-md-12">
        <div style="text-align:center;">
          <h3>VISI</h3>
          <P>Menjadi Gereja Yang Mandiri</p>
          <P>Missioner & Menjadi Berkat Bagi Masyarakat</p>
          <br><br>
          <h3>MISI</h3>
          <p>1.	Melaksanakan pelatihan penginjilan & penginjilan secara intensif</p>
          <p>2.	Melaksanakan pengutusan dalam rangka pembukaan jemaat baru</p>
          <p>3.	Melaksanakan pemuridan secara intensif untuk membangun jemaat yang berkualitas</p>
          <p>4.	Meningkatkan SDM anggota jemaat & masyarakat melalui pendidikan formal & non formal ( TK, Paud, Kursus Ketrampilan Praktis )</p>
          <p>5.	Mendirikan koperasi serba usaha untuk menggali potensi dan meningkatkan taraf hidup anggota jemaat serta masyarakat</p>
        </div>  
        </div>

      </div>
      <!-- /.row -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    &nbsp;
    &nbsp;
    <br><br>
    <br><br>
    <br><br>
    <br><br>
    <br><br>
    <br><br>
    <?php 
    require_once("footerpage.php");
?>

