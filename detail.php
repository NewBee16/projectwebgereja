<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_style.min.css" rel="stylesheet" type="text/css" />
    <style>
        a:hover {
            text-decoration: none;
        }
    </style>
<div class="col-md-12">
    <div class="col-md-6">
        <form asp-action="Approve"> 
            <div asp-validation-summary="ModelOnly" class="text-danger"></div>  
            <input type="hidden" asp-for="ID" />
            <div class="form-group-sm col-md-8">
                <label asp-for="UserID" class="control-label"></label>
                <input asp-for="UserID" class="form-control" disabled />
                <span asp-validation-for="UserID" class="text-danger"></span>
            </div>
            <div class="form-group col-md-4">
                <label class="control-label">Status:</label>             

                    <div class="btn btn-success" disabled>Approved</div>
            </div>
            <div class="form-group col-md-12" hidden>
                <label asp-for="EventID" class="control-label"></label>
                <input asp-for="EventID" class="form-control" disabled />
                <span asp-validation-for="EventID" class="text-danger"></span>
            </div>
            <div class="form-group col-md-12">
                <label asp-for="BuktiTransfer" class="control-label"></label>
                <img id="image" src="@imgUrl" class="img-thumbnail" onclick="openModal()" />
                <span asp-validation-for="BuktiTransfer" class="text-danger"></span>
            </div>
            <div class="form-group col-md-12">
               
             
                {
                    <a asp-action="KembaliKeList" class="pull-left w3-button w3-small w3-grey w3-text-white w3-hover-text-grey w3-hover-white">Batal</a>
                    <div class="pull-right">
                        <a asp-action="Delete" class="w3-button w3-small w3-red w3-text-white w3-hover-red w3-hover-white" asp-route-id="@Model.ID">Tolak</a>
                        <input type="submit" value="Approve" class="w3-button w3-small w3-blue w3-hover-text-blue w3-hover-white" />
                    </div>
                }
            </div>

            <div id="myModal" class="modal">
                <span class="close cursor" onclick="closeModal()">&times;</span>
                <div class="modal-content">
                    <img src="@imgUrl" style="width:100%">
                </div>
            </div>
        </form>
    </div>
</div>

@section Scripts {
    @{await Html.RenderPartialAsync("_ValidationScriptsPartial");}
    
}