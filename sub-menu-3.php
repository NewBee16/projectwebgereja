<?php 
  require_once("headerpage.php");
?>
    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Program Gereja
        <small>GBI Bukit Anugerah Anak Asuh</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Program Gereja Anak Asuh</li>
      </ol>

      <!-- Portfolio Item Row -->
      <div class="row">

        <div class="col-md-8">
        <img class="img-fluid" src="img/asuh1.jpg" alt="">
        </div>

        <div class="col-md-4">
          <h3 class="my-3">Anak Asuh</h3>
          <p>Dibuatnya program Anak Asuh dilatar belakangi oleh keprihatinan hamba Tuhan dan Gereja, melihat anak-anak Gunungkidul banyak yang putus sekolah dan ekonomi yang memprihatinka</p>
          <h3 class="my-3">Tujuan Program</h3>
          <ul>
            <li>Meningkatkan SDM Anak-anak Gunungkidul </li>
            <li>Mengurangi biaya pendidikan Anak-anak Gunungkidul</li>
            <li>Mengurangi angka anak putus sekolah di Gunungkidul</li>
          </ul>
        </div>

      </div>
      <!-- /.row -->

      <!-- Related Projects Row -->
      <h3 class="my-4">Dokumentasi Kegiatan</h3>

      <div class="row">

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/asuh2.jpg" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/asuh3.jpg" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/asuh4.jpg" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/asuh5.jpg" alt="">
          </a>
        </div>

      </div>
      <!-- /.row -->

    </div>
    </div>
    <!-- /.container -->

   <?php 
    require_once("footerpage.php");
?>
