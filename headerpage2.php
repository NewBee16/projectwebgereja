<?php 
   
    session_start();
    $username = $_SESSION['username'];
?> 

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Beranda - GBI BUKIT ANUGERAH</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index2.php">GBI BUKIT ANUGERAH</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
                <a class="nav-link" href="admin.php">Admin</a>
              </li>
              <li class="nav-item active">
                  <a class="nav-link" href="index2.php">Beranda</a>
                </li>

            <!-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Profil
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                <a class="dropdown-item" href="sejarah.php">Sejarah</a>
                <a class="dropdown-item" href="visimisi.php">Visi & Misi</a>
              </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Program Gereja
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                  <a class="dropdown-item" href="sub-menu-1.php">TK Bina Kasih</a>
                  <a class="dropdown-item" href="sub-menu-2.php">Sunem Ministry</a>
                  <a class="dropdown-item" href="sub-menu-3.php">Anak Asuh</a>
                  <a class="dropdown-item" href="sub-menu-4.php">Santunan Janda-Janda</a>
                  <a class="dropdown-item" href="sub-menu-5.php">Warung Sedekah</a>
                </div>
              </li> -->
                  
            <!-- <li class="nav-item active">
                <a class="nav-link" href="formpendaftaran2.php">Pendaftaran Anggota</a>
              </li> -->
            <!-- <li class="nav-item active">
                <a class="nav-link" href="contact.php">Kontak</a>
              </li> -->
              <li class="nav-item active">
                <a class="nav-link" href="proseslogout.php">
                <i class= "fa fa-fw fa-sign-out"></i>Keluar</a>
              </li>
              <li class="nav-item">
              <a class="nav" href="#">
              <?php 
               echo "Hallo ".$_SESSION["username"];
                ?>
                </a
               
              </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="content-wrapper">
    <div class="container-fluid">