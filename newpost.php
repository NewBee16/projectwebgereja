<?php
require_once ('headerpage.php');
?>
<?php 

$nomor = $_GET['nomor'];


require_once('koneksi.php');
$sql = "SELECT * FROM konten  WHERE nomor='$nomor'";
$result = $conn->query($sql);

?>

<?php if($result->num_rows>0){
  while($row=$result->fetch_assoc()){
    ?>
    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <!-- <?php echo $row['judul_konten'];?> -->
      <h1 class="mt-4 mb-3"><?php echo $row['judul_konten'];?></h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Renungan</li>
      </ol>

      <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

          <!-- Preview Image -->
          <img class="img-fluid rounded" src="./images/<?php echo $row['upload'];?>" alt="">
          

          <hr>
          <!-- Date/Time -->
          <p>Posting tanggal <?php echo $row["tanggal"];?></p>
          <td></td> 
          <hr>

          <!-- Post Content -->
         
            <p style="text-align: justify;">
            <?php echo $row['isi_konten']?>; </p>
              
          

          <div class="media mb-4">
            <div class="media-body">
              <div class="media mt-4">
              </div>
            </div>
          </div>
        </div>

  <?php }} ?>
        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Agenda -->
            <div class="card mb-4">
              <h5 class="card-header">Agenda Kegiatan</h5>
              <div class="card-body">
                <div class="input-group">
                    <div class="col-lg-12">
                        <li>30 Maret 2018   - Jumat Agung </li>
                        <li>31 Maret 2018   -  Paskah Gabungan</li>
                        <li>1 April 2018    - Paskah Anak Asuh</li>
                      </div>
                </div>
              </div>
            </div>
  
            <!-- Warta -->
            <div class="card my-4">
              <h5 class="card-header">Warta Gereja</h5>
              <div class="card-body">
                  <div class="col-lg-12">
                      <li>Rabu - 19.00 : Doa Rabu </li>
                      <li>Sabtu - 19.30 : Sarasehan</li>
                      <li>Minggu - 08.00 : Sekolah Minggu</li>
                      <li>Minggu - 09.00 : Ibadah</li>
                    </div>
              </div>
            </div>
  
          </div>
  
        </div>
        <!-- /.row -->
      </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2018</p>
      </div>

      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body> 