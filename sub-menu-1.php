
<!-- Head -->
  <?php 
  require_once("headerpage.php");
?>

  
    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Program Gereja
        <small>GBI Bukit Anugerah TK Bina Kasih</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Program Gereja TK Bina Kasih</li>
      </ol>

      <!-- Portfolio Item Row -->
      <div class="row">

        <div class="col-md-8">
          <img class="img-fluid" src="img/tk5.jpg" alt="">
        </div>

        <div class="col-md-4">
          <h3 class="my-3">TK Bina Kasih</h3>
          <p>Dibuatnya Pelayanan Sosial Pendidikan Taman Kanak-Kanak, karena di lingkungan gereja belum sama sekali ada pendidikan dasar, yaitu Taman kanak-kanak. Padahal anak-anak seusia TK banyak sekali. Hal ini juga yang mendorong adanya pelayanan pendidikan TK.</p>
          <h3 class="my-3">Tujuan Program</h3>
          <ul>
            <li>Untuk meningkatkan SDM anak-anak di Gunungkidul</li>
            <li>Untuk meningkatkan kecerdasan</li>
            <li>Untuk mengurangi anak putus sekolah</li>
          </ul>
        </div>

      </div>
      <!-- /.row -->

      <!-- Related Projects Row -->
      <h3 class="my-4">Dokumentasi Kegiatan</h3>

      <div class="row">

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
            <img class="img-fluid" src="img/tk1.jpg"alt="">
            
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
            <img class="img-fluid" src="img/tk2.jpg" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/tk3.jpg" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/tk4.jpg" alt="">
          </a>
        </div>

      </div>
      <!-- /.row -->

    </div>
    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php 
    require_once("footerpage.php");
?>
