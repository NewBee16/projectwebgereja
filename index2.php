<?php 
    require_once("headerpage2.php");
?>

<div style="  margin:-20px">
<div id="carouselExampleIndicators col-lg-12" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <!-- Slide One - Set the background image for this slide in the line below -->
    <div class="carousel-item active" style="background-image: url('images/image1.JPG')">
      <div class="carousel-caption d-none d-md-block">
        <h3>† GBI Bukit Anugerah †</h3>
      </div>
    </div>
    <!-- Slide Two - Set the background image for this slide in the line below -->
    <div class="carousel-item" style="background-image: url('images/image2.JPG')">
      <div class="carousel-caption d-none d-md-block">
        <h3>† GBI Bukit Anugerah †</h3>
      </div>
    </div>
    <!-- Slide Three - Set the background image for this slide in the line below -->
    <div class="carousel-item" style="background-image: url('images/image3.JPG')">
      <div class="carousel-caption d-none d-md-block">
        <h3>† GBI Bukit Anugerah †</h3>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
<br>
<!-- Page Content -->
<div class="container" style="margin:0px; width:100%;max-width:1280px;">
<!-- Page Heading/Breadcrumbs -->
<h1 class="my-4">Renungan</h1>
<!-- Breadcrumb -->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="index2.php">Beranda</a>
  </li>
 
  <li class="breadcrumb-item active">Renungan - 1</li>
</ol>

<div class="row">

  <!-- Blog Entries Column -->
  <div class="col-md-8">
  <?php 

require_once('koneksi.php');
$sql = "SELECT * FROM konten  WHERE id_konten='k02'";
$result = $conn->query($sql);

?>

<?php if($result->num_rows>0){
  while($row=$result->fetch_assoc()){
    ?>
    <!-- Blog Post -->
    <div class="card mb-4">
      <img class="img-fluid" src="./images/<?php echo $row['upload'];?>"" alt="Card image cap">
      <div class="card-body">
        <h2 class="card-title"><?php echo $row['judul_konten'];?>
        <p style="font-size:12px;"> <?php echo $row['username'];?></p>
        <p class="card-text"><?php echo $row['isi_konten']?>;</p>
        <a href="post-1.php" class="btn btn-primary">Lanjutkan Membaca &rarr;</a>
      </div>
      <div class="card-footer text-muted">
          Di posting tanggal :
          <td><?php echo $row["tanggal"];?></td> 
      </div>
    </div>
  <?php
  }
}
  ?>




    <!-- Pagination -->
    <ul class="pagination justify-content-center mb-4">
      <li class="page-item">
        <a class="page-link" href="#">&larr; Sebelumnya</a>
      </li>
      <li class="page-item enable">
        <a class="page-link" href="#">Berikutnya &rarr;</a>
      </li>
    </ul>

  </div>

  <!-- Sidebar Widgets Column -->
  <div class="col-md-4">

    <!-- Agenda -->
    <div class="card mb-4">
      <h5 class="card-header">Agenda Kegiatan</h5>
      <div class="card-body">
        <div class="input-group">
            <div class="col-lg-12">
                <li>30 Maret 2018   - Jumat Agung </li>
                <li>31 Maret 2018   -  Paskah Gabungan</li>
                <li>1 April 2018    - Paskah Anak Asuh</li>
              </div>
        </div>
      </div>
    </div>

    <!-- Warta -->
    <div class="card my-4">
      <h5 class="card-header">Warta Gereja</h5>
      <div class="card-body">
          <div class="col-lg-12">
              <li>Rabu - 19.00 : Doa Rabu </li>
              <li>Sabtu - 19.30 : Sarasehan</li>
              <li>Minggu - 08.00 : Sekolah Minggu</li>
              <li>Minggu - 09.00 : Ibadah</li>
            </div>
      </div>
    </div>

  </div>

</div>
<!-- /.row -->
</div>
</div>

<!-- /.container -->


<?php 
    require_once("footerpage.php");
?>
