<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GBI BUKIT ANUGERAH</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">

  </head>

  <body>

   <!-- Navigation -->
   <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.php">Beranda</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                  <a class="nav-link" href="index.php">Beranda</a>
                </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Profil
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                <a class="dropdown-item" href="blog-home-1.html">Sejarah</a>
                <a class="dropdown-item" href="blog-home-2.html">Visi & Misi</a>
              </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Program Gereja
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                  <a class="dropdown-item" href="portfolio-1-col.html">TK Bina Kasih</a>
                  <a class="dropdown-item" href="portfolio-2-col.html">Sunem Ministry</a>
                  <a class="dropdown-item" href="portfolio-3-col.html">Anak Asuh</a>
                  <a class="dropdown-item" href="portfolio-4-col.html">Santunan Janda2</a>
                  <a class="dropdown-item" href="portfolio-item.html">Warung Sedekah</a>
                </div>
              </li>
                  
            <li class="nav-item active">
                <a class="nav-link" href="#formanggota">Pendaftaran Anggota</a>
              </li>
            <li class="nav-item active">
                <a class="nav-link" href="contact.php">Contact</a>
              </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Kasih Tuhan</h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Renungan</li>
      </ol>

      <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

          <!-- Preview Image -->
          <img class="img-fluid rounded" src="img/kasih.jpg" alt="">

          <hr>
          <!-- Date/Time -->
          <p>Posted on January 1, 2017 at 12:00 PM By: Denny</p>
          <hr>

          <!-- Post Content -->
         
          <p>bacaan Alkitab : Roma 12:9-21 <br>
            <div style="text-align: justify;">Manusia adalah mahkluk sosial, artinya tidak bisa hidup sendiri tanpa memiliki hubungan dengan orang lain. Itu berarti seseorang akan menikmati dan menjalani kehidupanya sebagai manusia yang wajar jika ia memiliki hubungan dengan orang lain. Inilah yang membedakan antara manusia dan binatang. 
              Hidup mengasihi orang lain adalah ciri khas umat Allah. Perintah Allah yang utama adalah saling mengasihi, bukan semata-mata untuk menegaskan bahwa manusia itu adalah mahkluk sosial, tetapi karna Allah adalah kasih. Supaya dapat saling mengasihi secara baik dan benar, Yesus meminta umat-Nya untuk senantiasa mendasarkan diri pada hubungan yang erat dengan diri-Nya. Sebab Allah Sendiri melalui Yesus Kristus telah mengasihi umat-Nya dengan memberikan nyawa-Nya.
            </div> <br>
              <blockquote class="bl">
                  <p style="font-weight:bold"><em> di dalam Dia dan oleh darah-Nya kita beroleh penebusan, yaitu pengampunan dosa, menurut kekayaan kasih karunia-Nya
                      - Efesus 1:7</em></p>                  
                </blockquote>
              <div style="text-align: justify;">
              Mereka yang melakukan perintah ini, Yesus menyebutnya sebagai sahabat-Nya yang menunjukkan adanya keakraban, di antara Allah dan umat-Nya. Dalam keadaan seperti ini, umat Tuhan akan lebih memahami apa kehendak Tuhan dalam kehidupan mereka.
              Semua orang Kristen dari anak-anak sampai orang tua dapat dengan mudah berbicara tentang kasih walaupun kenyataannya kehidupan Kristen kita seringkali bertentangan dengan kasih. Ada sebagian orang yang berpikir bahwa ketika ia tidak punya masalah dengan orang lain, selalu tersenyum dan baik dengan orang lain maka ia sudah hidup dalam kasih. Namun lewat bacaan hari ini, kita akan belajar dari Paulus tentang bagaimana caranya seorang Kristen hidup dalam kasih.
            </div> <br>              
             <p>Ada beberapa hal yang mesti diperhatikan yaitu: <br> 
              jangan dengan pura-pura atau munafik,
              saling mengasihi diantara saudara, <br>
              mendahului dalam memberi hormat,<br>
              membantu orang-orang yang berkekurangan,<br>
              memberi tumpangan,<br>
              bersukacita dengan orang yang bersukacita,<br>
              menangis dengan orang yang menangis,<br>
              tidak membalas kejahatan dengan kejahatan,<br>
              tidak menganggap diri pandai dan <br>
              hidup dalam perdamaian dengan semua orang.
            </p>
            <div style="text-align: justify;"> 
              Dengan hidup saling mengasihi, umat Allah dapat menjalankan berbagai tugas panggilannya dengan baik dan “menghasilkan buah” bagi kemuliaan Allah dan kebahagian kehidupannya. Jemaat sebagai umat Allah, mesti menempatkan hal saling mengasihi sebagai ciri khas kehidupan. Jikalau tidak, maka sebenarnya kita sedang merusak Gereja-Jemaat itu sendiri. Bukankah banyak Gereja-Jemaat yang kalau berbicara tentang kasih begitu bersemangat tetapi untuk melaksanakannya begitu berat? Akibatnya banyak yang kehilangan jati dirinya, bahkan tidak berbeda dengan organisasi-organisasi lain dalam masyarakat.
              
              Ketika kita berusaha untuk memahami dan mengerti apa yang disampaikan Paulus tentang hidup dalam kasih, di sini, tentu lebih mudah dari melaksanakannya. Sebagai orang-orang yang percaya kepada Yesus Kristus, kita tidak punya pilihan lain kita harus hidup dalam kasih. Semoga kita tidak hanya pandai berbicara tentang kasih atau memahaminya saja tetapi lebih dari itu kita juga harus tinggal di dalam kasih itu.
            </div><br>
              <blockquote class="bl">
                  <p style="font-weight:bold"><em>kalau hal itu bergantung padamu, hiduplah dalam perdamaian dengan semua orang! - Roma 12:18</em></p>                  
                </blockquote>
              
          

          <div class="media mb-4">
            <div class="media-body">
              <div class="media mt-4">
              </div>
            </div>
          </div>
        </div>


        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Agenda -->
            <div class="card mb-4">
              <h5 class="card-header">Agenda Kegiatan</h5>
              <div class="card-body">
                <div class="input-group">
                    <div class="col-lg-12">
                        <li>30 Maret 2018   - Jumat Agung </li>
                        <li>31 Maret 2018   -  Paskah Gabungan</li>
                        <li>1 April 2018    - Paskah Anak Asuh</li>
                      </div>
                </div>
              </div>
            </div>
  
            <!-- Warta -->
            <div class="card my-4">
              <h5 class="card-header">Warta Gereja</h5>
              <div class="card-body">
                  <div class="col-lg-12">
                      <li>Rabu - 19.00 : Doa Rabu </li>
                      <li>Sabtu - 19.30 : Sarasehan</li>
                      <li>Minggu - 08.00 : Sekolah Minggu</li>
                      <li>Minggu - 09.00 : Ibadah</li>
                    </div>
              </div>
            </div>
  
          </div>
  
        </div>
        <!-- /.row -->
      </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
