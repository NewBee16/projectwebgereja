<?php 
require_once("headerpage.php");
// require_once("koneksi.php");

// $query = "SELECT jenis_konten,judul_konten,isi_konten FROM konten where nomor=1"; //You don't need a ; like you do in SQL
// $result = mysqli_query($conn,$query);
// $_row = mysqli_fetch_assoc($result);
?>
<br>    
<body>
    <h2>Form Ubah Konten</h2>
    <?php
        $connection = mysqli_connect('localhost', 'root', ''); //The Blank string is the password
        mysqli_select_db($connection,'projectgereja');
        $id= $_GET["nomor"];
        $konten= $_GET["konten"];
        $query = "SELECT * FROM konten where nomor='$id'"; //You don't need a ; like you do in SQL
        $result = mysqli_query($connection,$query);
        while($row = mysqli_fetch_array($result)){
    ?>
    <form action="prosesubahkonten.php" method="POST" enctype="multipart/form-data">
        <fieldset style="width:50%">
            <label for="konten">Jenis Konten :</label><br>
            <select name="konten">
                <option value="k01" <?php if ($konten == 'k01') echo ' selected="selected"';?>>profil</option>
                <option value="k02" <?php if ($konten == 'k02') echo ' selected="selected"';?>>renungan</option>
                <option value="k03" <?php if ($konten == 'k03') echo ' selected="selected"';?>>kontak</option>
                <option value="k04" <?php if ($konten == 'k04') echo ' selected="selected"';?>>program</option>
                <option value="k05" <?php if ($konten == 'k05') echo ' selected="selected"';?>>sidebar</option>
            </select><br><br>
            <input type='hidden' name='nomor' value="<?php echo $row["nomor"]?>">
            <label for='judul_konten'>Judul :</label><br/>
            <input type='text' name='judul_konten' value="<?php echo $row["judul_konten"]?>"><br/><br/>
            <label for='isi_konten'>Isi:</label>
            <textarea class='form-control' rows='5' name='isi_konten' ><?php echo $row["isi_konten"]?></textarea>
            <input type='file' name='fileToUpload'/>
            
            <button type="submit">Ubah</button>
            <button type="cancel">Cancel</button><br><br>
        </fieldset>
    </form>
        <?php }?>
</body>

<?php 
require_once("footerpage.php");
?>