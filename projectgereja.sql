-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 21 Apr 2018 pada 04.58
-- Versi Server: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projectgereja`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_konten`
--

CREATE TABLE `jenis_konten` (
  `id_konten` varchar(5) NOT NULL,
  `nama_konten` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_konten`
--

INSERT INTO `jenis_konten` (`id_konten`, `nama_konten`) VALUES
('k001', 'profil'),
('k002', 'renungan'),
('k003', 'kontak'),
('k004', 'program'),
('k005', 'sidebar');

-- --------------------------------------------------------

--
-- Struktur dari tabel `konten`
--

CREATE TABLE `konten` (
  `nomor` int(3) NOT NULL,
  `id_konten` varchar(5) NOT NULL,
  `id_admin` char(2) NOT NULL,
  `judul_konten` varchar(100) NOT NULL,
  `isi_konten` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `upload` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `member`
--

CREATE TABLE `member` (
  `id_user` int(2) NOT NULL,
  `username` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `telepon` varchar(13) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(8) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` char(1) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `kat_member` varchar(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `member`
--

INSERT INTO `member` (`id_user`, `username`, `nama`, `telepon`, `email`, `password`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `alamat`, `kat_member`) VALUES
(1, '', 'jordan', '085386177204', 'Theodorev52@gmail.com', '', 'Yogyakarta', '0000-00-00', 'L', 'demangan', 'jemaat'),
(2, '', 'jordan', '085386177204', 'Theodorev52@gmail.com', '', 'Yogyakarta', '0000-00-00', 'L', 'demangan', 'jemaat'),
(3, '', 'Yosephocy ', '12345', 'yoseph@si.ukdw.ac.id', '', 'Surakarta', '0000-00-00', 'P', 'Surga', 'jemaat'),
(4, '', 'asdasd', '', 'asdas', '', '', '0000-00-00', 'L', '', 'jemaat'),
(5, '', 'asdasd', '', 'asdas', '', '', '0000-00-00', 'L', '', 'jemaat'),
(6, '', 'ADASD', '', 'ASDASD', '', '', '0000-00-00', 'L', '', 'jemaat'),
(7, '', 'yofasfefo', 'afoaf', 'aeoifo', '', '', '0000-00-00', 'L', '', 'jemaat'),
(8, '', 'blalala`', '02387382`', 'tgjguj@yahoo.id', '', 'jogja', '0000-00-00', 'L', 'demngan', 'jemaat'),
(9, '', 'abcde', '019181717', 'theodore@gmail.com', '', 'jogja', '2012-12-01', 'P', 'demangan', 'jemaat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_log`
--

CREATE TABLE `tabel_log` (
  `tanggal` datetime NOT NULL,
  `id_admin` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jenis_konten`
--
ALTER TABLE `jenis_konten`
  ADD PRIMARY KEY (`id_konten`);

--
-- Indexes for table `konten`
--
ALTER TABLE `konten`
  ADD PRIMARY KEY (`nomor`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tabel_log`
--
ALTER TABLE `tabel_log`
  ADD PRIMARY KEY (`tanggal`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `konten`
--
ALTER TABLE `konten`
  MODIFY `nomor` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id_user` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
