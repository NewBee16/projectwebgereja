<?php 
    require_once("headerpage.php");
?>


    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Kontak
        <small>GBI Bukit Anugerah</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Kontak</li>
      </ol>

      <!-- Content Row -->
      <div class="row">
        <!-- Map Column -->
        <div class="col-lg-8 mb-4">
          <!-- Embedded Google Map -->
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3952.697934406176!2d110.5437446147784!3d-7.8217620943638115!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x452178322934e7d6!2sGBI+Bukit+Anugerah!5e0!3m2!1sid!2sid!4v1521412298456" width="700" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <!-- Contact Details Column -->
        <div class="col-lg-4 mb-4">
          <h3>Detail Kontak</h3>
          <p>
            Terbah
            <br>Patuk, Gunung Kidul, CA 90210
            <br>
          </p>
          <p>
            <abbr title="Phone">Telepon</abbr>: (0274) 456-7890
          </p>
        </div>
      </div>
      <!-- /.row -->

      <!-- Contact Form -->
      <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
            
      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

<?php 
    require_once("footerpage.php");
?>
