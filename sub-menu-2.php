<?php 
    require_once("headerpage.php");
?>
    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Program Gereja
        <small>GBI Bukit Anugerah Sunem Ministry</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Program Gereja Sunem Ministry</li>
      </ol>

      <!-- Portfolio Item Row -->
      <div class="row">

        <div class="col-md-8">
        <img class="img-fluid" src="img/sunem1.jpg" alt="">
        </div>

        <div class="col-md-4">
          <h3 class="my-3">Sunem Ministry</h3>
          <p>Dibuat program Sunem Ministry dilatar belakangi oleh kondisi kesejahteraan hamba Tuhan desa yang sangat memprihatinkan.</p>
          <h3 class="my-3">Tujuan Program</h3>
          <ul>
            <li>Hamba Tuhan desa bisa mandiri dan meningkatkan kesejahteraannya</li>
          </ul>
        </div>

      </div>
      <!-- /.row -->

      <!-- Related Projects Row -->
      <h3 class="my-4">Dokumentasi Kegiatan</h3>

      <div class="row">

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/sunem2.jpg" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/sunem3.jpg" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/sunem4.jpg" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
          <img class="img-fluid" src="img/sunem5.jpg" alt="">
          </a>
        </div>

      </div>
      <!-- /.row -->

    </div>
    </div>
    <!-- /.container -->

<?php 
    require_once("footerpage.php");
?>