<?php 
require_once("headerpage.php");
?>
 
  <body>
    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">
        <small>Sejarah GBI Bukit Anugerah </small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Sejarah GBI Bukit Anugerah</li>
      </ol>

      <!-- Portfolio Item Row -->
      <div class="row">

        
        <div class="col-md-12">
          <h3 class="my-6">Sejarah Gereja</h3>
          <p>Proses perjalanan perintisan Gereja Baptis Indonesia Kebayoran Cab. Bukit Anugerah dimulai sejak tahun 1991. Tepatnya tanggal 16 Agustus 1991, secara resmi perintisan jemaat/gereja Baptis Indonesia dimulai di Dukuh Belang, Desa Terbah, Kecamatan Patuk, Kabupaten Gunung Kidul. Waktu pembukaan pada saat itu dihadiri oleh jemaat Gereja Baptis Indonesia Anugerah Cab. Maguwoharjo yang sekarang telah menjadi gereja Induk dengan berubah nama menjadi Gereja Baptis Indonesia Anugerah Tuhan dan warga setempat serta dihadiri Bapak Kepala Dukuh setempat.
              Dalam pelayanan, gereja ini memiliki kegiatan yang awalnya bertempat di Alm. Bapak Sumoyoso yaitu :
              Sekolah minggu anak – anak dan kebaktian umum pada hari sabtu.
              Dalam proses perjalanan waktu rumah Alm. Bapaj Sumoyoso tidak muat lagi untuk menampung orang-orang yang beribadah. Pada akhirnya dengan hati yang tulus Alm. Bapak Sumoyono menyerahkan tanahnya seluas 350/m2 untuk bisa dibangun tempat ibadah yang permanen.
              Selama berbulan – bulan jemaat berdoa untuk tanah tersebut agar segera ada dana untuk membangun. Pada bulan Juli 1992 memulai membangun tempat ibadah. Berkat dukungan dana dari anak Tuhan dari Korea Selatan dan GGBI yang turut berperan dalam mendukung pendanaan, sehingga gereja bisa berdiri sampai sekarang ini.
              Bulan September tahun 1992 jemaat mulai memanfaatkan gedung yang baru untuk melaksanakan kegiatan Gereja. 
          </p>
        </div>

      </div>
      <!-- /.row -->

      <!-- Related Projects Row -->
      <h3 class="my-4">Dokumentasi</h3>

      <div class="row">

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
            <img class="img-fluid" src="http://placehold.it/500x300" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
            <img class="img-fluid" src="http://placehold.it/500x300" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
            <img class="img-fluid" src="http://placehold.it/500x300" alt="">
          </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
          <a href="#">
            <img class="img-fluid" src="http://placehold.it/500x300" alt="">
          </a>
        </div>

      </div>
      <!-- /.row -->

    </div>
    </div>
    <!-- /.container -->
    
    <br><br>
    <br><br>
    <br><br>
    <!-- Footer -->
    <?php 
    require_once("footerpage.php");
?>
